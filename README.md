
  

## Installation

-**php artisan db:seed**

-**php artisan send:notification {channel_type: sms | email} {count: integer} {--status=false | true , default: false}**

  
  

Before testing, please, run **php artisan queue:work** because notifications are collected in queue.

  
  

If you want to notify via SMS, 10 clients that are purchased products, use:

**php artisan sms 10 --status=false**

  
  

If you want to notify via EMAIL, 10 clients that are purchased products, use:

**php artisan email 10 --status=false**

  
  

If you want to renotify via SMS, 10, use:

**php artisan sms 10 --status=true**

  
  

If you want to renotify via EMAIL, 10, use:

**php artisan email 10 --status=true**

  Orders has API:
  url: {project_url}/api/orders
  can_handle_methods: POST | GET
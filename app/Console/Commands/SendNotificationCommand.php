<?php

namespace App\Console\Commands;

use App\Notification;
use App\Repositories\Actions\NotificationAction;
use Illuminate\Console\Command;
use App\Repositories\Adapter\NotificationAdapter;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class SendNotificationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:notification {channel_type} {limit} {--status=false}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Notification To Clients';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $notificationAdapter;

    protected $notificationActions;

    public function __construct(NotificationAdapter $notificationAdapter, NotificationAction $notificationActions)
    {
        parent::__construct();
        $this->notificationAdapter = $notificationAdapter;
        $this->notificationActions = $notificationActions;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!$this->validate($this->argument('channel_type'), $this->option('status'))) {
            return $this->warn('You need to provide Channel Type: sms or email');
        }
        $this->warn('We advice you, Before Sending Notifications, Run php artisan queue:work');
        $notification = $this->notificationAdapter->notify($this->argument('channel_type'));
        $notificationActions = $this->notificationActions->getNotificationActions();
        $notificationActions->setLimit(intval($this->argument('limit')));
        $notificationActions->setDeliveryStatus($this->option('status'));
        $notifications=$notificationActions->index();
        $countOfNotifications=$notifications->count();
        $this->info("Total Notifications: $countOfNotifications");
        $this->info($notification->sendNotification($notifications));
    }
    protected function validate($channel_type, $status)
    {
        if (
            in_array($channel_type, ['email', 'sms'])
            &&
            in_array($status, ['true', 'false'])
        ) {
            return true;
        }
        return false;
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Repositories\Actions\OrderAction;


class OrderController extends Controller
{


    protected $orderActions;
    public function __construct(OrderAction $orderActions)
    {
        $this->orderActions = $orderActions->getOrderActions();
    }
    public function index()
    {
        return $this->orderActions->index();
    }
    public function store(OrderRequest $request)
    {
        return $this->orderActions->store($request);
    }
}

<?php

namespace App\Listeners;

use App\Events\NotificationEvent;
use App\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotificationListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */

    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotificationEvent  $event
     * @return void
     */
    public function handle(NotificationEvent $event)
    {

        $order = $event->getOrder();
        $clientName = $order->client->name;
        $productName = $order->product->name;
        Notification::create([
            'order_id' => $order->id,
            'message' => "Dear $clientName, Thank you for purchasing for $productName"
        ]);
    }
}

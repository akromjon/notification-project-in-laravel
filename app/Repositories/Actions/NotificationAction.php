<?php

namespace App\Repositories\Actions;


use App\Repositories\Contracts\NotificationInterface;
use App\Repositories\Notifications\NotificationClass;

class NotificationAction
{
    public function getNotificationActions(): NotificationInterface
    {
        return new NotificationClass;
    }
}

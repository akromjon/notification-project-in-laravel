<?php
namespace App\Repositories\Actions;

use App\Order;
use App\Repositories\Contracts\OrderInterface;
use App\Repositories\Order\OrderClass;

class OrderAction{
    public function getOrderActions(): OrderInterface
    {
        return new OrderClass;
    }
}

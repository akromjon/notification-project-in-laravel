<?php
namespace App\Repositories\Adapter;

use App\Repositories\Contracts\SendNotificationInterface;

class NotificationAdapter{
    public function notify(string $type): SendNotificationInterface
    {
        if($type=='email') return new \App\Repositories\Notifications\EmailNotification;
        if($type=='sms')  return new \App\Repositories\Notifications\SmsNotification;
    }
}

<?php

namespace App\Repositories\Contracts;

interface NotificationInterface
{
    public function index(); 
    public function setDeliveryStatus(string $status): void;
    public function getDeliveryStatus(): bool;
    public function setLimit(int $limit);
    public function getLimit(): int;
}

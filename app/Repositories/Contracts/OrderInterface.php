<?php

namespace App\Repositories\Contracts;

interface OrderInterface
{
    public function index();
    public function store(object $order);
}

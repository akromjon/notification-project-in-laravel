<?php
namespace App\Repositories\Contracts;

interface  SendNotificationInterface{
    public function sendNotification(object $notifications);
}

<?php

namespace App\Repositories\Notifications;

use App\Repositories\Contracts\SendNotificationInterface;

class EmailNotification extends SendNotificationClass implements SendNotificationInterface
{
    const CHANNEL_TYPE = 'EMAIL';
    public function sendNotification(object $notifications)
    {
        foreach ($notifications as $notification) {
            /**
             * 
             * Mail Function needs to put here!
             */
            $this->updateNotification($notification, true, SELF::CHANNEL_TYPE);
        }
        return $this->message(SELF::CHANNEL_TYPE);
    }
}

<?php

namespace App\Repositories\Notifications;

use App\Repositories\Contracts\NotificationInterface;
use App\Notification;

class NotificationClass implements NotificationInterface
{
    protected $notification;
    protected $delivery_status = false;
    protected $limit = 5;
    public function __construct()
    {
        $this->notification = new Notification;
    }
    public function index()
    {
        return $this->notification::query()
            ->select('id', 'order_id', 'delivery_status', 'message', 'client_channel_type')
            ->where('delivery_status', $this->delivery_status)
            ->limit($this->limit)
            ->get();
    }
    public function setDeliveryStatus(string $delivery_status): void
    {        
        $this->delivery_status = strtolower($delivery_status) == 'true' ? true : false;;
    }
    public function getDeliveryStatus(): bool
    {
        return $this->delivery_status;
    }
    public function setLimit(int $limit)
    {
        $this->limit = $limit;
    }
    public function getLimit(): int
    {
        return $this->limit;
    }
}

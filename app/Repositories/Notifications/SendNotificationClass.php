<?php

namespace App\Repositories\Notifications;



class SendNotificationClass
{
    public function updateNotification(object $notification, bool $status, string $channel_type)
    {
        $notification->update(
            [
                'delivery_status' => $status,
                'client_channel_type' => $channel_type
            ]
        );
    }
    public function message(string $channel_type)
    {
        return 'Notification is sent Successfully to their '.$channel_type.'!';
    }
}

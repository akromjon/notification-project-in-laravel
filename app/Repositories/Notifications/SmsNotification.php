<?php

namespace App\Repositories\Notifications;

use App\Repositories\Contracts\SendNotificationInterface;

class SmsNotification extends SendNotificationClass implements SendNotificationInterface
{
    const CHANNEL_TYPE = 'SMS';
    public function sendNotification(object $notifications)
    {
        foreach ($notifications as $notification) {
            /**
             * Here, You can send sms. Developer needs to set up SmS to send!
             */

            // After sms is sent, Update Notification delivery status to False

            $this->updateNotification($notification, true, SELF::CHANNEL_TYPE);
        }
        return $this->message(SELF::CHANNEL_TYPE);
    }
}

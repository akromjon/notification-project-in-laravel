<?php

namespace App\Repositories\Order;

use App\Events\NotificationEvent;
use App\Order;
use App\Repositories\Contracts\OrderInterface;

class OrderClass implements OrderInterface
{
    protected $order;

    public function __construct()
    {
        $this->order = new Order;
    }
    public function index()
    {
        $orders =
            $this->order
            ->select('id', 'amount', 'product_id', 'client_id')
            ->get();
        return response()->json($orders);
    }
    public function store(object $request)
    {
        $order=$this->order->create($request->all());
        $notificationEvent=new NotificationEvent($order);
        event($notificationEvent);
        return response()->json(
            [
                'message' => 'Order has recorded',
                'status' => True,
            ]
        );
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use App\Client;
use App\Events\NotificationEvent;
use App\Order;
use App\Product;
use Illuminate\Support\Facades\Artisan;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if ($this->checkTableExists('clients')) {
            Client::truncate();
            $faker = Faker\Factory::create();
            for ($i = 0; $i < 10; $i++) {
                Client::create([
                    'name' => $faker->name,
                    'email' => $faker->email,
                    'phone_number' => $faker->randomNumber,
                ]);
            }
        }
        if ($this->checkTableExists('products')) {
            Product::truncate();
            for ($id = 0; $id < 10; $id++) {
                Product::create([
                    'name' => 'Samsung-version' . $id,
                    'amount' => 699 + $id
                ]);
            }
        }
        if ($this->checkTableExists('orders')) {
            Order::truncate();
            for ($id = 0; $id < 10; $id++) {
                $order = Order::create([
                    'client_id' => $id,
                    'product_id' => $id,
                    'amount' => 59999 + $id,
                ]);
                $notification = new NotificationEvent($order);
                event($notification);
            }
        }
    }

    protected function checkTableExists($tableName)
    {
        return (Schema::hasTable($tableName));
    }
}
